# Introduction to Algorithmic Differentation
This repository contains all tutorial exercises for the course *Introduction to Algorithmic Differentiation*. Make sure to follow the steps below to setup a working configuration. 

For a local install, you can skip the cluster section and go directly to [setting up the project](#setting-up-the-project).

# Using the cluster
Most of the information for registering/accessing the cluster can be found in the [IT Center Wiki](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/).

## Registering for the cluster
To access the cluster, please first register an account using [RegApp](https://regapp.itc.rwth-aachen.de/). See [here](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/49b19247a5fb4e0282a9a8757db5b012/) for details.

## Accessing the cluster
You can access the cluster via ssh:

```bash
ssh <TIM>@login23-1.hpc.itc.rwth-aachen.de
```

where `<TIM>` should be replaced by the number you also use for the username to Moodle when signing in with SSO.

## Saving ssh keys
To not have to re-enter the password/two-factor auth after a first login, ensure that you have setup and linked your ssh key to the service.
Under `Index/Übersicht` -> `My SSH Pubkeys` you can add an ssh key from your device (usually under `~/.ssh/<key>.pub`). 
Then going back to the main view, you can `Set SSH Key` for the RWTH HPC service and add your keys to the service.

# Setting up the project

The provided script `setup.sh` has everything we need to setup the project. Make sure you have CMake installed and inside
your PATH (by default the case on the cluster).

So:

```bash
./setup.sh
```

which simply runs:

```bash
cmake --preset linux-gcc-debug
```

Other presets (for different compiler or compilation options) can be queried using
```bash
cmake --list-presets
```

## Working on a solution
### Your solutions
We recommend you to create a new branch for your own solutions.

```bash
git checkout -b mysolution
```

You can then save your own solution by commiting to this branch.

### Applying the provided solution
After the tutorial solution was presented, you will receive a patch with the solution. To apply it, run:

```bash
git apply solution<idx>.patch
```

## Compiling and running a tutorial exercise
The script `run.sh` simplifies the process of compiling and running only a single tutorial. Use:
```bash
./run.sh <tutorial_number>
```

with `<tutorial_number>` replaced by, e.g., 1 and CMake will only compile and run the target for tutorial 1.

