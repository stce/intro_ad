#!/bin/sh

# Build and run tutorial exercise using: ./run.sh tutorial_name
# E.g., for tutorial 3: ./run.sh 3

cmake --build build --target $1 && ./build/tutorial/$1/$1
