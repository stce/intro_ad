#! /usr/bin/env gnuplot

plot 'data.dat' using 1:2 with lines title "f"
replot 'data.dat' using 1:3 with lines title "f (smooth)"

pause mouse close
