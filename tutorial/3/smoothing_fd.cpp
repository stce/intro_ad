// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 3             //
//                                                                            //
//                   Finite Differences & Sigmoid Smoothing                   //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include C++ standard header
#include <cmath>
#include <cstdint>
#include <iostream>

using namespace std;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Calculate the (non-differentiable) primal function.
 *
 * @param[in] x Input value x.
 * @returns Result of primal function.
 ******************************************************************************/
double f(const double x) {
  return 0.0;  // TODO: Exercise 1
}

// ******************************* Smoothing ******************************** //

/******************************************************************************
 * @brief Calculate sigmoid function dependent on input value, position of the
 *        non-differentiability and the smoothing factor.
 *
 * @param[in] x Input value x.
 * @param[in] p Position of the non-differentiability.
 * @param[in] w Smoothing factor.
 * @returns Result of sigmoid function.
 ******************************************************************************/
double s(const double x, const double p, const double w) {
  return 0.0;
}

/******************************************************************************
 * @brief Calculate smoothed function dependent on input value and the
 *        smoothing factor.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns Result of smoothed primal function.
 ******************************************************************************/
double fs(const double x, const double w) {
  return cos(x);  // TODO: Exercise 1
}

// ************************** Analytical Gradients ************************** //

/******************************************************************************
 * @brief Calculate the analytical gradient of the primal function.
 *
 * @param[in] x Input value x.
 * @returns The analytical gradient of the primal function.
 ******************************************************************************/
double dfdx(double x) {
  return 0.0;  // TODO: Exercise 3
}

/******************************************************************************
 * @brief Calculate the analytical gradient of the sigmoid function.
 *
 * @param[in] x Input value x.
 * @param[in] p Position of the non-differentiability.
 * @param[in] w Smoothing factor.
 * @returns The analytical gradient of the sigmoid function.
 ******************************************************************************/
double dsdx(double x, const double p, const double w) {
  return 0.0;  // TODO: Exercise 3
}

/******************************************************************************
 * @brief Calculate the analytical gradient of the smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns The analytical gradient of the smoothed primal function.
 ******************************************************************************/
double dfsdx(double x, const double w) {
  return 0.0;  // TODO: Exercise 3
}

// *************************** Finite Differences *************************** //

/******************************************************************************
 * @brief Calculate the one-sided finite differences gradient of the
 *        smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns The one-sided finite differences of the smoothed primal function.
 ******************************************************************************/
double dfsdx_fd1(double x, const double w) {
  return 0.0;  // TODO: Exercise 4
}

/******************************************************************************
 * @brief Calculate the central finite differences gradient of the
 *        smoothed function.
 *
 * @param[in] x Input value x.
 * @param[in] w Smoothing factor.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
double dfsdx_fd2(double x, const double w) {
  return 0.0;  // TODO: Exercise 4
}

// ********************************** Main ********************************** //

int main() {
  double w = 0.05;

  for (int i = -50; i <= 50; i++) {
    double x = 0.1 * i;
    double f_res = f(x);
    double fs_res = fs(x, w);
    double dfdx_res = dfdx(x);
    double dfsdx_res = dfsdx(x, w);
    double dfsdx_fd1_res = dfsdx_fd1(x, w);
    double dfsdx_fd2_res = dfsdx_fd2(x, w);

    std::cout << x;
    std::cout << " " << f_res;
    std::cout << " " << fs_res;
    std::cout << " " << dfdx_res;
    std::cout << " " << dfsdx_res;
    std::cout << " " << dfsdx_fd1_res;
    std::cout << " " << std::fabs(dfsdx_fd1_res - dfsdx_res) / std::fabs(dfsdx_res);
    std::cout << " " << dfsdx_fd2_res;
    std::cout << " " << std::fabs(dfsdx_fd2_res - dfsdx_res) / std::fabs(dfsdx_res);
    std::cout << std::endl;
  }

  return 0;
}
