// A simplified implementation of a multi-layer perceptron (feedforward neural-network).

#include <Eigen/Dense>

#include <ad/ad.h>

template<typename T>
T smooth_relu(T x) {
  T one = 1;
  return x / (one + exp(-(x)));
}

template<typename T>
void mlp(int width, int depth, const Eigen::VectorX<T> &x, const Eigen::VectorX<T> &p,
         Eigen::VectorX<T> &y) {
  int n = x.size();
  int m = y.size();
  assert(p.size() == width * (n + 1 + (width + 1) * (depth - 1) + m) + m &&
         "dimension issue in mlp()");
  Eigen::VectorX<T> v(width);
  int pc = 0;
  // input layer
  for (int j = 0; j < width; ++j) {
    for (int i = 0; i < n; ++i) {
      v[j] = v[j] + p[pc++] * x[i];
    }
    v[j] = smooth_relu(v[j]) + p[pc++];
  }
  // intermediate layers
  for (int k = 0; k < depth - 1; ++k) {
    Eigen::VectorX<T> v_new(width);
    for (int j = 0; j < width; ++j) {
      for (int i = 0; i < width; ++i) {
        v_new[j] = v_new[j] + p[pc++] * v[i];
      }
    }
    for (int j = 0; j < width; ++j) {
      v[j] = smooth_relu(v_new[j]) + p[pc++];
    }
  }
  // output layer
  for (int j = 0; j < m; ++j) {
    y[j] = 0;
    for (int i = 0; i < width; ++i) {
      y[j] = y[j] + p[pc++] * v[i];
    }
    y[j] = smooth_relu(y[j]) + p[pc++];
  }
}
