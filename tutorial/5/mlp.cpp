// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 5             //
//                                                                            //
//                        First-Order Derivative Models                       //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include "mlp.h"

#include <Eigen/Dense>

#include <ad/ad.h>
#include <cassert>
#include <iostream>
#include <limits>

using std::cin;
using std::cout;
using std::endl;

template<typename T>
using vector_t = Eigen::VectorX<T>;

/******************************************************************************
 * @brief Validate the differential invariant.
 *
 * @param[in] x_t1 Directional derivative x^(1) (seed).
 * @param[in] x_a1 Directional derivative x_(1) (harvested).
 * @param[in] y_t1 Directional derivative y^(1) (harvested).
 * @param[in] y_a1 Directional derivative y_(1) (seed).
 * @returns true if the invariant holds, false otherwise.
 ******************************************************************************/
template<typename T>
bool differential_invariant(vector_t<T> x_t1, vector_t<T> x_a1, T y_t1, T y_a1) {
  // TODO: Exercise 3
  return false;
}

int main() {
  int n, m, width, depth;
  cout << "n=";
  cin >> n;
  assert(n > 0);
  cout << "m=";
  cin >> m;
  assert(m > 0);
  cout << "width=";
  cin >> width;
  assert(width > 0);
  cout << "depth=";
  cin >> depth;
  assert(depth > 0);
  int n_p = width * (n + 1 + (width + 1) * (depth - 1) + m) + m;

  // dummy input data
  vector_t<float> x_v = vector_t<float>::Random(n);
  // dummy parameters (i.e. weights)
  vector_t<float> p_v = vector_t<float>::Random(n_p);

  // Dummy primal evaluation
  {
    vector_t<float> y(m);
    mlp(width, depth, x_v, p_v, y);

    cout << "MLP output:" << endl;
    for (int i = 0; i < m; ++i) {
      cout << y(i) << endl;
    }
  }

  // Tangent mode
  {
    using T = ad::tangent_t<float>;
    vector_t<T> x(n), y(m), p(n_p);

    // TODO: Exercise 1
  }

  // Adjoint mode
  {
    using T = ad::adjoint_t<float>;
    vector_t<T> x(n), y(m), p(n_p);

    // TODO: Exercise 2
  }

  // Compare last entry using the differential invariant
  {
    // TODO: Exercise 3
  }

  return 0;
}
