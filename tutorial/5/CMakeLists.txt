cmake_path(GET CMAKE_CURRENT_SOURCE_DIR STEM tutorial)
add_executable(${tutorial} mlp.cpp)
target_compile_features(${tutorial} PRIVATE cxx_std_20)
target_link_libraries(${tutorial} PRIVATE ${PROJECT_NAME})
