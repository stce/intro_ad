// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 8             //
//                                                                            //
//                        Second-Order Derivative Models                      //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include "mlp.h"

#include <Eigen/Dense>

#include <ad/ad.h>
#include <cassert>
#include <chrono>
#include <iostream>

using std::cout;
using std::endl;

template<typename T>
using vector_t = Eigen::VectorX<T>;

template<typename T>
using matrix_t = Eigen::MatrixX<T>;

int width = 5;
int depth = 3;

/******************************************************************************
 * @brief Driver to calculate the tangent of the MLP.
 * @param[in] x Input vector.
 * @param[in] p Parameters vector (i.e., the weights and biases).
 * @param[out] y_v Output value vector.
 * @param[out] y_x Output tangent vector for differentiation w.r.t. x.
 * @param[out] y_p Output tangent vector for differentiation w.r.t. p.
 * @param[in] wrt_p Whether to differentiate w.r.t. p (true) or x (false).
 */
template<typename T>
void tangent_of_mlp(vector_t<T> x_v, vector_t<T> p_v, vector_t<T> &y_v, matrix_t<T> &y_x,
                    matrix_t<T> &y_p, bool wrt_p) {

  using TT = ad::tangent_t<T>;
  int n = x_v.size();
  int m = y_v.size();
  int n_p = p_v.size();
  vector_t<TT> x(n), y(m), p(n_p);

  for (int i = 0; i < n; ++i) {
    x(i).value() = x_v(i);
  }

  for (int i = 0; i < n_p; ++i) {
    p(i).value() = p_v(i);
  }

  if (!wrt_p) {
    for (int i = 0; i < x.size(); ++i) {
      // seed
      x(i).tangent() = 1;
      mlp(width, depth, x, p, y);
      // harvest
      for (int j = 0; j < y.size(); ++j) {
        y_x(j, i) = y(j).tangent();
      }
      // unseed
      x(i).tangent() = 0;
    }
  }

  if (wrt_p) {
    for (int i = 0; i < p.size(); ++i) {
      // seed
      p(i).tangent() = 1;
      mlp(width, depth, x, p, y);
      // harvest
      for (int j = 0; j < y.size(); ++j) {
        y_p(j, i) = y(j).tangent();
      }
      // unseed
      p(i).tangent() = 0;
    }
  }

  for (int i = 0; i < m; ++i) {
    y_v(i) = y(i).value();
  }
}

/******************************************************************************
 * @brief Driver to calculate the adjoint of the MLP.
 * @param[inout] x Input vector.
 * @param[in] p Parameters vector (i.e., the weights and biases).
 * @param[out] y_v Output value vector.
 * @param[out] x_a Output adjoint vector for differentiation w.r.t. x.
 * @param[out] p_a Output adjoint vector for differentiation w.r.t. p.
 * @param[in] wrt_p Whether to differentiate w.r.t. p (true) or x (false).
 */
template<typename T>
void adjoint_of_mlp(vector_t<T> &x_v, vector_t<T> p_v, vector_t<T> &y_v, matrix_t<T> &x_a,
                    matrix_t<T> &p_a, bool wrt_p) {

  using AT = ad::adjoint_t<T>;
  int n = x_v.size();
  int m = y_v.size();
  int n_p = p_v.size();
  vector_t<AT> x(n), y(m), p(n_p);

  for (int i = 0; i < n; ++i) {
    x(i).value() = x_v(i);
    if (!wrt_p)
      x(i).register_input();
  }
  for (int i = 0; i < n_p; ++i) {
    p(i).value() = p_v(i);
    if (wrt_p)
      p(i).register_input();
  }

  mlp(width, depth, x, p, y);

  for (int j = 0; j < y.size(); ++j) {
    y_v(j) = y(j).value();
  }

  for (int j = 0; j < y.size(); ++j) {
    AT::tape::init_adjoints();
    y(j).adjoint() = 1;
    AT::tape::interpret();
    if (!wrt_p) {
      for (int i = 0; i < x.size(); ++i) {
        x_a(j, i) = x(i).adjoint();
      }
    }
    if (wrt_p) {
      for (int i = 0; i < p.size(); ++i) {
        p_a(j, i) = p(i).adjoint();
      }
    }
  }
  AT::tape::reset();  // enable repeated calls
}

enum mode { tangent, adjoint };

/******************************************************************************
 * @brief Driver to calculate the tangent-of-x of the MLP w.r.t. p.
 * @param[in] x Input vector.
 * @param[in] p Parameters vector (i.e., the weights and biases).
 * @param[out] yv Output value vector.
 * @param[out] dydpv Output jacobian w.r.t p.
 * @param[out] ddydpp Output Hessian w.r.t p.
 * @param[in] first_order_mode Whether to use tangent or adjoint mode for the first-order model.
 */
void ddFdpp(vector_t<double> xv, vector_t<double> pv, vector_t<double> &yv, matrix_t<double> &dydpv,
            vector_t<matrix_t<double>> &ddydpp, mode first_order_mode) {
  int n = xv.size(), m = yv.size(), n_p = pv.size(), i, j, k;
  vector_t<ad::tangent_t<double>> x(n), y(m), p(n_p);
  matrix_t<ad::tangent_t<double>> dydx(m, n);
  matrix_t<ad::tangent_t<double>> dydp(m, n_p);

  // TODO: Exercise 1 and 2
}

/******************************************************************************
 * @brief Driver to calculate the adjoint-of-tangent of the MLP w.r.t. x.
 * @param[in] x Input vector.
 * @param[in] p Parameters vector (i.e., the weights and biases).
 * @param[out] yv Output value vector.
 * @param[out] dydxv Output jacobian w.r.t x.
 * @param[out] ddydxx Output Hessian w.r.t x.
 */
void ddFdxx(vector_t<double> xv, vector_t<double> pv, vector_t<double> &yv, matrix_t<double> &dydxv,
            vector_t<matrix_t<double>> &ddydxx) {
  int n = xv.size(), m = yv.size(), n_p = pv.size();
  using T1 = ad::adjoint_t<double>;
  vector_t<T1> x(n), y(m), p(n_p);
  matrix_t<T1> dydx(m, n);
  matrix_t<T1> dydp(m, n_p);
  int j, i, i1, i2;

  // TODO: Exercise 3
}

void print_jacobian(const matrix_t<double> &jacobian) {
  cout << jacobian << endl;
}

void print_hessian(const vector_t<matrix_t<double>> &hessian) {
  int i = 0;
  do {
    cout << hessian(i) << endl;
    i = i + 1;
  } while (i < hessian.size());
}

void init_hessian(vector_t<matrix_t<double>> &hessian, int n) {
  int i = 0;
  do {
    hessian(i) = matrix_t<double>::Zero(n, n);
    i = i + 1;
  } while (i < hessian.size());
}

int main() {
  int n = 5;
  int m = 2;
  bool print = true;

  int n_p = width * (n + 1 + (width + 1) * (depth - 1) + m) + m;

  // dummy input data
  vector_t<double> x = vector_t<double>::Random(n);
  // dummy parameters (i.e. weights)
  vector_t<double> p = vector_t<double>::Random(n_p);

  // output
  vector_t<double> y(m);

  // jacobian (w.r.t. p)
  matrix_t<double> dydp(m, n_p);

  // hessian (w.r.t. p)
  vector_t<matrix_t<double>> ddydpp(m);

  init_hessian(ddydpp, n_p);

  // TODO: Exercise 1
  //
  // ddFdpp(x, p, y, dydp, ddydpp, mode::adjoint);
  // print_jacobian(dydp);
  // print_hessian(ddydpp);

  // TODO: Exercise 2
  //
  // ddFdpp(x, p, y, dydp, ddydpp, mode::tangent);
  // print_jacobian(dydp);
  // print_hessian(ddydpp);

  // TODO: Exercise 3

  // jacobian (w.r.t. x)
  matrix_t<double> dydx(m, n);

  // hessian (w.r.t. x)
  vector_t<matrix_t<double>> ddydxx(m);

  init_hessian(ddydxx, n);

  // ddFdxx(x, p, y, dydx, ddydxx);
  // print_jacobian(dydx);
  // print_hessian(ddydxx);

  return 0;
}
