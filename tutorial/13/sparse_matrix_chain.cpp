// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 13            //
//                                                                            //
//                        Dynamic Programming for SMCBP                       //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include <Eigen/Dense>

#include <cassert>
#include <iostream>

template<typename T>
using matrix_t = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

template<typename T>
using vector_t = Eigen::Matrix<T, Eigen::Dynamic, 1>;

/******************************************************************************
 * @brief Count the number of fmas and store the sparsity pattern of the
 *        product of two sub-chains A and B in C, if not yet done.
 *
 * @param[in] A Input matrix A.
 * @param[in] B Input matrix B.
 * @param[in] C Input/Output matrix C.
 * @returns total fma cost.
 ******************************************************************************/
template<typename T>
T cnt_fma_and_cmp_sparsity(matrix_t<T> A, matrix_t<T> B, matrix_t<T> &C) {
  T fma = 0;
  // TODO: Exercise 2
  return fma;
}

// TODO: Exercise 3: adjust for sparse matrix format

// NOTE: The logic in dp remains the same as in the dense case except for the
// differing local costs to merge two sub-chains

/******************************************************************************
 * @brief Calculate the minimal fma cost of the given sparse matrix chain product.
 *
 * @param[in] A Input matrix A. // TODO: Modify to be of type matrix_t<matrix_t<T>>
 * @returns minimal fma cost.
 ******************************************************************************/
template<typename T>
T dp(matrix_t<T> A, matrix_t<vector_t<T>> &C) {
  // TODO: Exercise 4: Adjust assertions
  int p = A.rows();
  assert(p > 0);
  assert(A.cols() == 2);
  assert(p == C.rows());
  assert(C.rows() == C.cols());
  for (int j = 0; j < p; j = j + 1) {
    for (int i = j; i >= 0; i = i - 1)
      if (i == j)
        C(j, i) = vector_t<T>::Zero(2);
      else
        for (int k = i + 1; k <= j; k = k + 1) {
          // TODO: Exercise 4: Adjust costs
          T cost = C(j, k)(0) + C(k - 1, i)(0) + A(j, 0) * A(k, 1) * A(i, 1);
          if (k == i + 1 || cost < C(j, i)(0)) {
            C(j, i)(0) = cost;
            C(j, i)(1) = k;
          }
        }
  }
  return C(p - 1, 0)(0);
}

int main() {
  using T = unsigned long;
  // Matrix Chain Product as sequence of sparse matrices
  int p;
  std::cin >> p;
  assert(p > 0);

  // Table to store all sparsity patterns (same p x p triangular matrix as below)
  // list with coordinate pairs to store sparsity pattern in coordinate format
  // TODO: A should be of type matrix_t<matrix_t<T>>
  matrix_t<T> A(p, 2);
  for (int i = 0; i < p; i = i + 1) {
    // TODO: Exercise 1: Change read of matrix chain A
    std::cin >> A(i, 0) >> A(i, 1);
  }

  std::cout << "Matrix Chain Product:" << std::endl;
  for (int i = p - 1; i >= 0; i = i - 1)
    std::cout << "A(" << i << "): " << A(i, 0) << " x " << A(i, 1) << std::endl;
  // Dynamic Programming Table as // p x p lower triangular matrix
  // storing optimal cost and split position per subchain
  matrix_t<vector_t<T>> C(p, p);
  for (int i = 0; i < p; i = i + 1)
    for (int j = 0; j < p; j = j + 1)
      C(i, j) = vector_t<T>::Zero(2);
  dp(A, C);
  std::cout << "Dynamic Programming Table:" << std::endl;
  for (int j = 0; j < p; j = j + 1)
    for (int i = j - 1; i >= 0; i = i - 1) {
      std::cout << "Cost(C(" << j << "," << i << "))=" << C(j, i)(0) << "; " << "Split before A("
                << C(j, i)(1) << "); " << std::endl;
    }
  return 0;
}
