// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 9        //
//                                                                            //
//                           More 2nd-Order Models                            //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include "lighthouse.h"

// Include Eigen
#include <Eigen/Dense>
#include <ad/ad.h>

// Include C++ standard header
#include <iostream>

using namespace std;

/******************************************************************************
 * @brief Calculate the value, Jacobian and Hessian of the lighthouse function
 *        via adjoint over adjoint mode.
 *
 * @param[in] x Input vector x.
 * @param[out] y The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 * @param[out] H The Hessian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void ddFdxx_aoa(
    const Eigen::Vector<T0, N> &x_v,
    Eigen::Vector<T0, M> &y_v,
    Eigen::Matrix<T0, M, N> &J,
    Eigen::Vector<Eigen::Matrix<T0, N, N>, M> &H) {

  using T1 = ad::adjoint_t<T0>;
  using T2 = ad::adjoint_t<T1>;

  // Indices in the adjoint over adjoint model (scalar mode)
  int j, i1, i2;

  // Todo: Exercise 4

  T1::tape::reset();
  T2::tape::reset();
}

/******************************************************************************
 * @brief Calculate the value, Jacobian and Hessian of the lighthouse function
 *        via adjoint over adjoint mode.
 *
 * @param[in] x Input vector x.
 * @param[out] y The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 * @param[out] H The Hessian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void d2Rdx2_aoa_vector(
    const Eigen::Vector<T0, N> &x_v,
    Eigen::Vector<T0, M> &y_v,
    Eigen::Matrix<T0, M, N> &J,
    Eigen::Vector<Eigen::Matrix<T0, M, N>, N> &H) {

  // Vector sizes
  const int M1 = 2;
  const int M2 = 2;

  using T1 = ad::adjoint_t<T0, M2>;
  using T2 = ad::adjoint_t<T1, M1>;

  // Indices in the adjoint over adjoint model (vector mode)
  int j, i1, i2, mu1, mu2;

  // Todo: Exercise 5

  T1::tape::reset();
  T2::tape::reset();
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  const Eigen::Vector<double, N> &x = Eigen::Vector<double, N>::Random();
  Eigen::Vector<double, M> y;
  Eigen::Matrix<double, M, N> J;
  Eigen::Vector<Eigen::Matrix<double, N, N>, M> H;

  ddFdxx_aoa(x, y, J, H);
  std::cout << "x:\n" << x << "\n\n";
  std::cout << "y:\n" << y << "\n\n";
  std::cout << "J:\n" << J << "\n\n";
  std::cout << "H_0:\n" << H(0) << "\n\n";
  std::cout << "H_1:\n" << H(1) << "\n\n";
  return 0;
}
