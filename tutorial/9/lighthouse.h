// naumann@stce.rwth-aachen.de

#include <cmath>
#include "Eigen/Dense"

using namespace std;

namespace {
  const int N = 4;
  const int M = 2;
}

/******************************************************************************
 * @brief Lighthouse function.
 *
 * @param[in] x Input vector x (size 4).
 * @param[out] y Output vector y (size 2).
 ******************************************************************************/
template <typename T> void F(
  const Eigen::Vector<T, N> &x,
  Eigen::Vector<T, M> &y
) {
  T v = x(2) * x(3);
  v = sin(v) / cos(v);
  T w = x(1) - v;
  y(0) = x(0) * v/w;
  y(1) = y(0) * x(1);
}
