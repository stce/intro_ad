// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 4        //
//                                                                            //
//       Multivariate Finite Differences & 2nd Order Finite Differences       //
//                                                                            //
// Copyright (C) 2023 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Include Eigen
#include <Eigen/Dense>

// Include C++ standard header
#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

// ****************************** Custom types ****************************** //

//! Vector with 2 entries (inputs & Gradient)
using vec2_t = Eigen::Vector2<double>;

//! Matrix with 2x2 entries (Hessian)
using mat2_t = Eigen::Matrix2<double>;

// ***************************** Primal function **************************** //

/******************************************************************************
 * @brief Evaluate the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns Result of primal function.
 ******************************************************************************/
double R(const vec2_t &x) {
  return 0.0;
}

// ************************* Analytical Derivatives ************************* //

/******************************************************************************
 * @brief Calculate the analytical Gradient of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The analytical Gradient of the Rosenbrock function.
 ******************************************************************************/
vec2_t dRdx(const vec2_t &x) {
  vec2_t g;
  return g;
}

/******************************************************************************
 * @brief Calculate the analytical Hessian of the Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The analytical Hessian of the Rosenbrock function.
 ******************************************************************************/
mat2_t d2Rdx2(const vec2_t &x) {
  mat2_t H;
  return H;
}

// *************************** Finite Differences *************************** //

/******************************************************************************
 * @brief Calculate the central finite differences Gradient of the
 *        Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
vec2_t dRdx_fd(const vec2_t &x) {
  vec2_t g;

  return g;
}

/******************************************************************************
 * @brief Calculate the central finite differences Hessian of the
 *        Rosenbrock function.
 *
 * @param[in] x Input vector x.
 * @returns The central finite differences of the smoothed primal function.
 ******************************************************************************/
mat2_t d2Rdx2_fd(const vec2_t &x) {
  mat2_t H;

  return H;
}

// ****************************** Optimization ****************************** //

/******************************************************************************
 * @brief Newton's algorithm to find the optimum of the Rosenbrock function.
 *
 * @param[in] x0 Initial guess.
 * @param[in] accuracy Accuracy threshold. Algorithm stops if the norm of
 *                     the gradient falls below this accuracy.
 * @returns The optimmum (minimum) of the Rosenbrock function, if successful.
 ******************************************************************************/

vec2_t newton(const vec2_t &x0, const double accuracy = 1e-6) {
  vec2_t x = x0;
  return x;
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  vec2_t x0 = {-0.5, 2};
  vec2_t x_min_2 = newton(x0);
  std::cout << "Rosenbrock minimum: " << x_min_2.transpose() << "\n";

  return 0;
}
