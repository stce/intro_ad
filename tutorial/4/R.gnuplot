#!/usr/bin/env gnuplot

set xyplane relative 0
set ztics norangelimit logscale autofreq
set title "Rosenbrock Function"
set xlabel "x"
set xrange [-1.5:1.5]
set ylabel "y"
set yrange [-0.5:1.5]
set zlabel "Z"
set logscale z 10
set isosamples 50, 50

set contour base
set cntrparam levels disc 100,10,1,0.1
set style fill transparent solid 0.6

Rosenbrock(x,y) = (1-x)**2 + 100*(y - x**2)**2
splot Rosenbrock(x,y)

pause mouse close
