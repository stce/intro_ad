// A simplified implementation of a multi-layer perceptron (feedforward neural-network).

#include <Eigen/Dense>

#include <ad/ad.h>

template<typename T>
T smooth_relu(T x) {
  T one = 1;
  return x / (one + exp(-(x)));
}

/******************************************************************************
 * @brief Banded multi-layer perceptron (MLP).
 * @param[in] depth The number of layers of the MLP.
 * @param[in] kl The number of sub-diagonals in the weigth matrices.
 * @param[in] ku The number of super-diagonals in the weigth matrices.
 * @param[in] x Input vector.
 * @param[in] p Parameters vector (i.e., the weights and biases).
 * @param[out] y Output value vector.
 * @param[in] arrowhead Whether to create an arrowhead pattern in the Jacobian.
 */
template<typename T>
void banded_mlp(int depth, int kl, int ku,
    const Eigen::VectorX<T> &x, const Eigen::VectorX<T> &p,
    Eigen::VectorX<T> &y, bool arrowhead = false) {

  int n = x.size();
  int m = y.size();
  assert(n == m && "Input and output vectors need to have the same size");

  assert(kl >= 0 && "Amount of subdiagonals needs to be >= 0");
  assert(ku >= 0 && "Amount of superdiagonals needs to be >= 0");
  kl = std::min(kl, n-1);
  ku = std::min(ku, n-1);
  int bandwidth = kl + ku + 1;

  int p_layer = bandwidth * n - (kl * (kl + 1)) / 2 - (ku * (ku + 1)) / 2 + n;
  assert(p.size() >= depth * p_layer && "Parameter vector is too small");

  y = x;
  int pc = 0;

  // Layers
  for (int k = 0; k < depth; ++k) {
    Eigen::VectorX<T> y_new(n);
    for (int j = 0; j < n; ++j) {
      for (int i = std::max(j-kl, 0); i <= std::min(j+ku, n-1); ++i) {
        y_new[j] = y_new[j] + p[pc++] * y[i];
      }
    }
    for (int j = 0; j < n; ++j) {
      y[j] = smooth_relu(y_new[j]) + p[pc++];
    }
  }

  // Create arrowhead pattern if requested
  if (arrowhead) {
    for (int j = 0; j < n; ++j) {
      y[j] = y[j] + static_cast<T>(2) * x[0]; // First row of the Jacobian
    }
    y[0] = y[0] - x.sum(); // First column of the Jacobian
  }
}
