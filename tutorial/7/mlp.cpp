// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 7             //
//                                                                            //
//                          Jacobian Compression II:                          //
//            Bidirectional Compression & Linearity & Vector Modes            //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include "mlp.h"

#include <Eigen/Dense>

#include <ad/ad.h>
#include <cassert>
#include <chrono>
#include <iostream>
#include <limits>

using std::cin;
using std::cout;
using std::endl;

template<typename T>
using vector_t = Eigen::VectorX<T>;

template<typename T>
using matrix_t = Eigen::MatrixX<T>;

/******************************************************************************
 * @brief Driver to calculate the Jacobian of the MLP in tangent mode.
 * @param[in] depth The number of layers of the MLP.
 * @param[in] kl The number of sub-diagonals in the weigth matrices.
 * @param[in] ku The number of super-diagonals in the weigth matrices.
 * @param[in] x_v Input vector.
 * @param[in] p_v Parameters vector (i.e., the weights and biases).
 * @param[out] y_v Output value vector.
 * @param[out] dydx Output Jacobian for differentiation w.r.t. x.
 * @param[in] arrowhead Whether to create an arrowhead pattern in the Jacobian.
 */
template<typename T>
void tangent_of_mlp(int depth, int kl, int ku, vector_t<T> x_v, vector_t<T> p_v, vector_t<T> &y_v,
                    matrix_t<T> &dydx, bool arrowhead = false) {

  using TT = ad::tangent_t<T>;
  int n = x_v.size();
  int n_p = p_v.size();
  vector_t<TT> x(n), y(n), p(n_p);

  for (int i = 0; i < n; ++i) {
    x(i).value() = x_v(i);
  }

  for (int i = 0; i < n_p; ++i) {
    p(i).value() = p_v(i);
  }

  for (int i = 0; i < n; ++i) {
    // seed
    x(i).tangent() = 1;
    banded_mlp(depth, kl, ku, x, p, y, arrowhead);
    // harvest
    for (int j = 0; j < n; ++j) {
      dydx(j, i) = y(j).tangent();
    }
    // unseed
    x(i).tangent() = 0;
  }

  for (int i = 0; i < n; ++i) {
    y_v(i) = y(i).value();
  }
}

/******************************************************************************
 * @brief Driver to calculate the Jacobian of the MLP in adjoint mode.
 * @param[in] depth The number of layers of the MLP.
 * @param[in] kl The number of sub-diagonals in the weigth matrices.
 * @param[in] ku The number of super-diagonals in the weigth matrices.
 * @param[in] x_v Input vector.
 * @param[in] p_v Parameters vector (i.e., the weights and biases).
 * @param[out] y_v Output value vector.
 * @param[out] dydx Output adjoint vector for differentiation w.r.t. x.
 * @param[in] arrowhead Whether to create an arrowhead pattern in the Jacobian.
 */
template<typename T>
void adjoint_of_mlp(int depth, int kl, int ku, vector_t<T> x_v, vector_t<T> p_v, vector_t<T> &y_v,
                    matrix_t<T> &dydx, bool arrowhead = false) {

  using AT = ad::adjoint_t<T>;
  int n = x_v.size();
  int n_p = p_v.size();
  vector_t<AT> x(n), y(n), p(n_p);

  for (int i = 0; i < n; ++i) {
    x(i).value() = x_v(i);
    x(i).register_input();
  }
  for (int i = 0; i < n_p; ++i) {
    p(i).value() = p_v(i);
  }

  banded_mlp(depth, kl, ku, x, p, y, arrowhead);

  for (int j = 0; j < n; ++j) {
    y_v(j) = y(j).value();
  }

  for (int j = 0; j < n; ++j) {
    AT::tape::init_adjoints();
    y(j).adjoint() = 1;
    AT::tape::interpret();
    for (int i = 0; i < n; ++i) {
      dydx(j, i) = x(i).adjoint();
    }
  }
}

int main() {
  int n, kl, ku, depth;
  bool print, arrowhead;
  std::string answer;
  cout << "print? [y/N] ";
  std::getline(std::cin, answer);
  print = answer.starts_with("y");
  cout << "create arrowhead pattern? [y/N] ";
  std::getline(std::cin, answer);
  arrowhead = answer.starts_with("y");
  cout << "n = ";
  cin >> n;
  assert(n > 0);
  cout << "kl = ";
  cin >> kl;
  assert(n > 0);
  cout << "ku = ";
  cin >> ku;
  assert(n > 0);
  cout << "L (depth) = ";
  cin >> depth;
  assert(depth > 0);

  int bandwidth = kl + ku + 1;
  int p_layer = bandwidth * n - (kl * (kl + 1)) / 2 - (ku * (ku + 1)) / 2 + n;
  int n_p = depth * p_layer;

  // dummy input data
  vector_t<float> x_v = vector_t<float>::Random(n);
  // dummy parameters (i.e. weights)
  vector_t<float> p_v = vector_t<float>::Random(n_p);

  // output values from tangent
  vector_t<float> y_v_tangent(n);
  // output values from adjoint
  vector_t<float> y_v_adjoint(n);

  // resulting tangent (w.r.t x)
  matrix_t<float> dydx_tangent(n, n);
  // resulting adjoint (w.r.t x)
  matrix_t<float> dydx_adjoint(n, n);

  using timer = std::chrono::high_resolution_clock;
  auto print_time = [](timer::time_point start, timer::time_point end) {
    cout << "time: " << std::chrono::duration<double, std::milli>(end - start).count() << " ms\n"
         << endl;
  };

  // Tangent mode
  {
    cout << "starting tangent mode\n" << endl;
    auto start = timer::now();
    tangent_of_mlp(depth, kl, ku, x_v, p_v, y_v_tangent, dydx_tangent, arrowhead);
    auto end = timer::now();
    if (print) {
      std::cout << "y:\n" << y_v_tangent << "\n\n";
      std::cout << "dy/dx\n" << dydx_tangent << "\n\n";
    }
    cout << "end tangent mode" << endl;
    print_time(start, end);
  }

  // Adjoint mode
  {
    cout << "starting adjoint mode\n" << endl;
    auto start = timer::now();
    adjoint_of_mlp(depth, kl, ku, x_v, p_v, y_v_adjoint, dydx_adjoint, arrowhead);
    auto end = timer::now();

    if (print) {
      std::cout << "y:\n" << y_v_adjoint << "\n\n";
      std::cout << "dy/dx\n" << dydx_adjoint << "\n\n";
    }
    cout << "end adjoint mode" << endl;
    print_time(start, end);
  }

  // check that values match
  assert((y_v_tangent - y_v_adjoint).norm() < std::numeric_limits<float>::epsilon());

  return 0;
}
