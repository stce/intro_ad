// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 11            //
//                                                                            //
//                 Adjoint Single-Assignment Straight-Line Code               //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include "lighthouse.h"

// Include Eigen
#include <Eigen/Dense>

#include <ad/ad.h>

// Include C++ standard header
#include <iostream>
#include <limits>

using namespace std;

/******************************************************************************
 * @brief Calculate the value and Jacobian of the lighthouse function
 *        via tangent SAC.
 *
 * @param[in] x_v Input vector x.
 * @param[out] y_v The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void dFdx_sac_t1(const Eigen::Vector<T0, N> &x_v, Eigen::Vector<T0, M> &y_v,
                 Eigen::Matrix<T0, M, N> &J) {
  int j, i;

  Eigen::Vector<T0, N> x_t1 = Eigen::Vector<T0, N>::Zero();
  Eigen::Vector<T0, M> y_t1 = Eigen::Vector<T0, M>::Zero();

  i = 0;
  do {
    // Seed X^(1)
    x_t1(i) = 1.0;

    F_sac_t1(x_v, x_t1, y_v, y_t1);

    // Harvest Jacobian
    j = 0;
    do {
      J(j, i) = y_t1(j);
      j = j + 1;
    } while (j < M);

    // Reset X^(1)
    x_t1(i) = 0.0;

    i = i + 1;
  } while (i < N);
}

/******************************************************************************
 * @brief Calculate the value and Jacobian the lighthouse function
 *        via adjoint SAC.
 *
 * @param[in] x_v Input vector x.
 * @param[out] y_v The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void dFdx_sac_a1(const Eigen::Vector<T0, N> &x_v, Eigen::Vector<T0, M> &y_v,
                 Eigen::Matrix<T0, M, N> &J) {
  // TODO: Exercise 2
}

template<typename T>
T g(T x) {
  T z = sin(x);
  return z * z;
}

template<typename T>
T f(Eigen::VectorX<T> x) {
  T y = 0;
  int i = 0;
  do {
    if (i > x.size()) {
      break;
    }

    if (cos(x(i)) > 0) {
      y = y + g(x(i) * x(i));
    } else {
      y = y + y * g(x(i - 1) * x(i));
    }
    i = i + 1;
  } while (y < 1);

  return y;
}

/******************************************************************************
 * @brief Calculate the value and gradient of the example function f
 *        via adjoint SAC.
 *
 * @param[in] x Input vector x.
 * @param[out] y The output value.
 * @param[out] dydx The output gradient.
 ******************************************************************************/
template<typename T>
void dfdx_sac_a1(Eigen::VectorX<T> x, T &y, Eigen::VectorX<T> &dydx) {
  // TODO: Exercise 3
}

/******************************************************************************
 * @brief Calculate the value and gradient of the example function f
 *        via central finite differences (for comparison with adjoint SAC).
 *
 * @param[in] x Input vector x.
 * @param[out] y The output value.
 * @param[out] dydx The output gradient.
 ******************************************************************************/
template<typename T>
void f_cfd(Eigen::VectorX<T> x, T &y, Eigen::VectorX<T> &dydx) {
  T h = std::sqrt(std::numeric_limits<T>::epsilon());
  y = f(x);

  // TODO: Exercise 3
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  {
    Eigen::Vector<double, N> x = Eigen::Vector<double, N>::Random();
    Eigen::Vector<double, M> y, y_vec, y_sac;
    Eigen::Matrix<double, M, N> J, J_vec, J_sac;

    dFdx_sac_t1(x, y_sac, J_sac);
    std::cout << "Tangent mode (SAC):\n\n";
    std::cout << "y:\n" << y_sac << "\n";
    std::cout << "J:\n" << J_sac << "\n\n";

    dFdx_sac_a1(x, y, J);
    std::cout << "Adjoint mode (SAC):\n\n";
    std::cout << "y:\n" << y << "\n";
    std::cout << "error: " << (y - y_sac).norm() << "\n\n";
    std::cout << "J:\n" << J << "\n";
    std::cout << "error: " << (J - J_sac).norm() << "\n\n";
  }
  {
    Eigen::VectorX<double> x(2);
    x << 1, 2;

    Eigen::VectorX<double> dydx_cfd(2);
    double y_cfd;
    f_cfd(x, y_cfd, dydx_cfd);
    std::cout << "Central-Finite Differences:\n\n";
    std::cout << "    y: " << y_cfd << std::endl;
    std::cout << "    dydx:" << dydx_cfd.transpose() << "\n" << std::endl;

    Eigen::VectorX<double> y_sac(2);
    double y;
    dfdx_sac_a1(x, y, y_sac);
    std::cout << "Adjoint mode (SAC):\n\n";
    std::cout << "    y: " << y << std::endl;
    std::cout << "    dydx:" << y_sac.transpose() << std::endl;
    std::cout << "error: " << (dydx_cfd - y_sac).norm() << "\n\n";
  }

  return 0;
}
