// naumann@stce.rwth-aachen.de

#include "Eigen/Dense"

#include <cmath>

using namespace std;

namespace {
const int N = 4;
const int M = 2;
}  // namespace

/******************************************************************************
 * @brief Lighthouse function.
 *
 * @param[in] x Input vector x (size 4).
 * @param[out] y Output vector y (size 2).
 ******************************************************************************/
template<typename T>
void F(const Eigen::Vector<T, N> &x, Eigen::Vector<T, M> &y) {
  T v = x(2) * x(3);
  v = sin(v) / cos(v);
  T w = x(1) - v;
  y(0) = x(0) * v / w;
  y(1) = y(0) * x(1);
}

/******************************************************************************
 * @brief Lighthouse function SAC.
 *
 * @param[in] x Input vector x (size 4) (ν, γ, ω, t).
 * @param[out] y Output vector y (size 2).
 ******************************************************************************/
template<typename T>
void F_sac(const Eigen::Vector<T, N> &x, Eigen::Vector<T, M> &y) {
  T v0 = x(0);  // ν
  T v1 = x(1);  // γ
  T v2 = x(2);  // ω
  T v3 = x(3);  // t

  T v4 = v2 * v3;
  T v5 = sin(v4);
  T v6 = cos(v4);
  T v7 = v5 / v6;
  T v8 = v1 - v7;
  T v9 = v0 * v7;
  T v10 = v9 / v8;
  T v11 = v10 * v1;

  y(0) = v10;
  y(1) = v11;
}

/******************************************************************************
 * @brief Lighthouse function SAC first order tangent model.
 *
 * @param[in] x Input vector x (size 4) (ν, γ, ω, t).
 * @param[in] x_t1 Tangent input seeds.
 * @param[out] y Output vector y (size 2).
 * @param[out] y_t1 Output tangent (to harvest from).
 ******************************************************************************/
template<typename T>
void F_sac_t1(const Eigen::Vector<T, N> &x, const Eigen::Vector<T, N> &x_t1, Eigen::Vector<T, M> &y,
              Eigen::Vector<T, M> &y_t1) {
  T v0 = x(0);
  T v0_t1 = x_t1(0);
  T v1 = x(1);
  T v1_t1 = x_t1(1);
  T v2 = x(2);
  T v2_t1 = x_t1(2);
  T v3 = x(3);
  T v3_t1 = x_t1(3);

  T v4 = v2 * v3;
  T v4_t1 = v2_t1 * v3 + v2 * v3_t1;

  T v5 = sin(v4);
  T v5_t1 = cos(v4) * v4_t1;

  T v6 = cos(v4);
  T v6_t1 = -sin(v4) * v4_t1;

  T v7 = v5 / v6;
  T v7_t1 = v5_t1 / v6 - (v5 * v6_t1) / pow(v6, 2);

  T v8 = v1 - v7;
  T v8_t1 = v1_t1 - v7_t1;

  T v9 = v7 / v8;
  T v9_t1 = v7_t1 / v8 - (v7 * v8_t1) / pow(v8, 2);

  T v10 = v0 * v9;
  T v10_t1 = v0_t1 * v9 + v0 * v9_t1;

  T v11 = v10 * v1;
  T v11_t1 = v10_t1 * v1 + v10 * v1_t1;

  y(0) = v10;
  y_t1(0) = v10_t1;
  y(1) = v11;
  y_t1(1) = v11_t1;
}

/******************************************************************************
 * @brief Lighthouse function SAC first order adjoint model.
 *
 * @param[in] x Input vector x (size 4) (ν, γ, ω, t).
 * @param[out] x_a1 Adjoint output.
 * @param[out] y Output vector y (size 2).
 * @param[in] y_a1 Adjoint seed vector.
 ******************************************************************************/
template<typename T>
void F_sac_a1(const Eigen::Vector<T, N> &x, Eigen::Vector<T, N> &x_a1, Eigen::Vector<T, M> &y,
              const Eigen::Vector<T, M> &y_a1) {
  // TODO: Exercise 1
}
