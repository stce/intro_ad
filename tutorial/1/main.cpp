// ************************************************************************** //
//       Introduction to Algorithmic Differentiation - Tutorial 1             //
//                                                                            //
//                   Getting Started & Essential Linear Algebra               //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

// Quick reference at: https://eigen.tuxfamily.org/dox/group__QuickRefPage.html
#include <Eigen/Dense>

#include <iostream>

/******************************************************************************
 * @brief Calculate the condition number of a matrix.
 *
 * @param[in] A Input matrix A.
 * @returns condition number.
 ******************************************************************************/
double cond(const Eigen::MatrixXd &A) {
  return 0.0;  // TODO: Exercise 1
}

/******************************************************************************
 * @brief Check whether the matrix A is invertible.
 *
 * @param[in] A Input matrix A.
 * @returns true if A is invertible, false otherwise.
 ******************************************************************************/
bool is_invertible(const Eigen::MatrixXd &A) {
  return false;  // TODO: Exercise 2
}

/******************************************************************************
 * @brief Solve the Linear System Ax = b using an LU decomposition.
 *
 * @param[in] A Input matrix A.
 * @param[in] b Input rhs vector b.
 * @returns solution vector x.
 ******************************************************************************/
Eigen::VectorXd lu_solve(Eigen::MatrixXd &A, Eigen::VectorXd &b) {
  return {};  // TODO: Exercise 3
}

/******************************************************************************
 * @brief Compute the residual of Ax = b.
 *
 * @param[in] A Input matrix A.
 * @param[in] x Solution vector x.
 * @param[in] b Input rhs vector b.
 * @returns residual vector.
 ******************************************************************************/
Eigen::VectorXd residual(Eigen::MatrixXd &A, Eigen::VectorXd &x, Eigen::VectorXd &b) {
  return {};  // TODO: Exercise 4
}

/******************************************************************************
 * @brief Fill in a matrix with random values.
 *
 * @param[in] A Input matrix A.
 * @returns condition number.
 ******************************************************************************/
template<typename T>
void random(Eigen::MatrixBase<T> &A) {
  // TODO: Exercise 5
}

/******************************************************************************
 * @brief Prints information about the matrix A and vector b.
 *
 * @param[in] A Input matrix A.
 * @param[in] b Input rhs vector b.
 ******************************************************************************/
void info(const Eigen::MatrixXd &A, const Eigen::VectorXd &b) {
  std::cout << "A:\n" << A << std::endl;
  std::cout << "b:\n" << b << std::endl;
  std::cout << "cond(A): " << cond(A) << std::endl;
  std::cout << "A invertible? " << (is_invertible(A) ? "yes" : "no") << std::endl;
}

/******************************************************************************
 * @brief Prints solution and residual
 *
 * @param[in] A Input matrix A.
 * @param[in] b Input rhs vector b.
 ******************************************************************************/
void report(Eigen::MatrixXd &A, Eigen::VectorXd &b) {
  std::cout << "---------" << std::endl;
  Eigen::MatrixXd A_ = A;
  Eigen::VectorXd b_ = b;
  Eigen::VectorXd x = lu_solve(A_, b_);
  std::cout << "x:\n" << x << std::endl;
  std::cout << "residual:\n" << residual(A, x, b) << std::endl;

  // TODO: Exercise 6: solve Ax=b using LU decomposition from Eigen
  // x = ?
  std::cout << "x (from Eigen):\n" << x << std::endl;
}

int main() {
  {
    std::cout << "---- Small example: ----" << std::endl;
    Eigen::MatrixXd A{{3., 2., -1.}, {2., -2., 4.}, {-1., .5, -1.}};
    Eigen::VectorXd b{{1., -2., 0.}};
    info(A, b);
    report(A, b);
  }

  {
    std::cout << "---- Random example: ----" << std::endl;
    const int n = 5;
    Eigen::MatrixXd A(n, n);
    Eigen::VectorXd b(n);
    random(A);
    random(b);

    info(A, b);
    report(A, b);
  }
  return 0;
}
