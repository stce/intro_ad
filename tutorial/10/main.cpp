// ************************************************************************** //
//       Introduction to Algorithmic Differentiation SS23 - Tutorial 9        //
//                                                                            //
//                       Higher Order Derivative Models                       //
//                                                                            //
// Copyright (C) 2024 Software and Tools for Computational Engineering (STCE) //
//               RWTH Aachen University - www.stce.rwth-aachen.de             //
// ************************************************************************** //

#include "lighthouse.h"

// Include Eigen
#include <Eigen/Dense>
#include <ad/ad.h>

// Include C++ standard header
#include <iostream>

using namespace std;

/******************************************************************************
 * @brief Calculate the value, Jacobian and Hessian of the lighthouse function
 *        via adjoint over adjoint mode.
 *
 * @param[in] x_v Input vector x.
 * @param[out] y_v The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 * @param[out] H The Hessian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void ddFdxx_aoa(
    const Eigen::Vector<T0, N> &x_v,
    Eigen::Vector<T0, M> &y_v,
    Eigen::Matrix<T0, M, N> &J,
    Eigen::Vector<Eigen::Matrix<T0, N, N>, M> &H) {

  // This type (its tape) contains the second-order derivatives!
  using T1 = ad::adjoint_t<T0>;
  // This type (its tape) contains the first-order derivatives!
  using T2 = ad::adjoint_t<T1>;

  // Indices in the adjoint over adjoint model
  int j, i1, i2;

  Eigen::Vector<T2, N> x;
  Eigen::Vector<T2, M> y;
  i1 = 0;
  do {
    // Set passive value
    x(i1).value().value() = x_v(i1);

    // Register value on second-order tape (T1::tape)
    x(i1).value().register_input();

    // Register variable on first-order tape (T2::tape)
    x(i1).register_input();

    i1 = i1 + 1;
  } while (i1 < N);

  // Evaluate Lighthouse function
  F(x, y);

  j = 0;
  do {
    // Harvest value
    y_v(j) = y(j).value().value();

    // Seed first-order tape
    T2::tape::init_adjoints();
    y(j).adjoint().value() = 1;
    T2::tape::interpret();

    i1 = 0;
    do {
      // Harvest Jacobian
      J(j,i1) = x(i1).adjoint().value();

      // Seed second-order tape
      T1::tape::init_adjoints();
      x(i1).adjoint().adjoint() = 1;

      // Interpret second-order tape and harvest Hessian
      T1::tape::interpret();

      i2 = i1;  // Exploit symmetry
      do {
        H(j)(i1, i2) = x(i2).value().adjoint();
        H(j)(i2, i1) = H(j)(i1, i2);
        i2 = i2 + 1;
      } while (i2 < N);

      i1 = i1 + 1;
    } while (i1 < N);

    j = j + 1;
  } while (j < M);

  T1::tape::reset();
  T2::tape::reset();
}

/******************************************************************************
 * @brief Calculate the value, Jacobian and Hessian of the lighthouse function
 *        via adjoint over adjoint mode.
 *
 * @param[in] x_v Input vector x.
 * @param[out] y_v The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 * @param[out] H The Hessian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void ddFdxx_aoa_vector(
    const Eigen::Vector<T0, N> &x_v,
    Eigen::Vector<T0, M> &y_v,
    Eigen::Matrix<T0, M, N> &J,
    Eigen::Vector<Eigen::Matrix<T0, N, N>, M> &H) {

  // Vector sizes
  const int M1 = 2;
  const int M2 = 2;

  // This type (its tape) contains the second-order derivatives!
  using T1 = ad::adjoint_t<T0, M2>;
  // This type (its tape) contains the first-order derivatives!
  using T2 = ad::adjoint_t<T1, M1>;

  // Indices in the adjoint over adjoint model
  int j, i1, i2, mu1, mu2;

  Eigen::Vector<T2, N> x;
  Eigen::Vector<T2, M> y;
  i1 = 0;
  do {
    // Set passive value
    x(i1).value().value() = x_v(i1);

    // Register value on second-order tape (T1::tape)
    x(i1).value().register_input();

    // Register variable on first-order tape (T2::tape)
    x(i1).register_input();

    i1 = i1 + 1;
  } while (i1 < N);

  // Evaluate Lighthouse function
  F(x, y);

  j = 0;
  do {
    T2::tape::init_adjoints();
    mu1 = 0;
    do {
      // Harvest value
      y_v(j + mu1) = y(j + mu1).value().value();

      // Seed first-order tape  ( Y_{1}_{mu1, j} )
      y(j + mu1).adjoint(mu1).value() = 1;
      mu1 = mu1 + 1;
    } while (mu1 < M1 && j + mu1 < M);

    T2::tape::interpret();

    mu1 = 0;
    do {
      i1 = 0;
      do {
        T1::tape::init_adjoints();
        mu2 = 0;
        do {
          // Harvest Jacobian ( X_{1}_{mu1, i1} )
          J(j + mu1, i1 + mu2) = x(i1 + mu2).adjoint(mu1).value();

          // Seed second-order tape ( X_{1,2}_{mu2, mu1, i1} )
          x(i1 + mu2).adjoint(mu1).adjoint(mu2) = 1;
          mu2 = mu2 + 1;
        } while (mu2 < M2 && i1 + mu2 < N);

        // Interpret second-order tape and harvest Hessian
        T1::tape::interpret();

        i2 = i1;  // Exploit symmetry
        do {
          mu2 = 0;
          do {
            H(j + mu1)(i1 + mu2, i2) = x(i2).value().adjoint(mu2);
            H(j + mu1)(i2, i1 + mu2) = H(j + mu1)(i1 + mu2, i2);
            mu2 = mu2 + 1;
          } while (mu2 < M2 && i1 + mu2 < N);
          i2 = i2 + 1;
        } while (i2 < N);
        i1 = i1 + mu2;
      } while (i1 < N);
      mu1 = mu1 + 1;
    } while (mu1 < M1 && j + mu1 < M);
    j = j + mu1;
  } while (j < M);

  T1::tape::reset();
  T2::tape::reset();
}

/******************************************************************************
 * @brief Calculate the value, Jacobian and Hessian of the lighthouse function
 *        via tangent over tangent SAC.
 *
 * @param[in] x_v Input vector x.
 * @param[out] y_v The value of the lighthouse function.
 * @param[out] J The Jacobian of the lighthouse function.
 * @param[out] H The Hessian of the lighthouse function.
 ******************************************************************************/
template<typename T0>
void ddFdxx_sac_t1_t2(
    const Eigen::Vector<T0, N> &x_v,
    Eigen::Vector<T0, M> &y_v,
    Eigen::Matrix<T0, M, N> &J,
    Eigen::Vector<Eigen::Matrix<T0, N, N>, M> &H) {

  // Indices in the adjoint over adjoint model
  int j, i1, i2;

  // TODO: Exercise 4
}

// ********************************** Main ********************************** //

int main(const int argc, const char *argv[]) {
  const Eigen::Vector<double, N> &x = Eigen::Vector<double, N>::Random();
  Eigen::Vector<double, M> y, y_vec, y_sac;
  Eigen::Matrix<double, M, N> J, J_vec, J_sac;
  Eigen::Vector<Eigen::Matrix<double, N, N>, M> H, H_vec, H_sac;

  std::cout << "x:\n" << x << "\n\n";

  ddFdxx_aoa(x, y, J, H);
  std::cout << "Adjoint over Adjoint mode:\n\n";
  std::cout << "y:\n" << y << "\n\n";
  std::cout << "J:\n" << J << "\n\n";
  std::cout << "H_0:\n" << H(0) << "\n\n";
  std::cout << "H_1:\n" << H(1) << "\n\n";

  ddFdxx_aoa_vector(x, y_vec, J_vec, H_vec);
  std::cout << "Adjoint over Adjoint mode (vector):\n\n";
  std::cout << "y:\n" << y_vec << "\n";
  std::cout << "error: " << (y - y_vec).norm() << "\n\n";
  std::cout << "J:\n" << J_vec << "\n";
  std::cout << "error: " << (J - J_vec).norm() << "\n\n";
  std::cout << "H_0:\n" << H_vec(0) << "\n";
  std::cout << "error: " << (H(0) - H_vec(0)).norm() << "\n\n";
  std::cout << "H_1:\n" << H_vec(1) << "\n";
  std::cout << "error: " << (H(1) - H_vec(1)).norm() << "\n\n";

  ddFdxx_sac_t1_t2(x, y_sac, J_sac, H_sac);
  std::cout << "Tangent over Tangent mode (SAC):\n\n";
  std::cout << "y:\n" << y_sac << "\n";
  std::cout << "error: " << (y - y_sac).norm() << "\n\n";
  std::cout << "J:\n" << J_sac << "\n";
  std::cout << "error: " << (J - J_sac).norm() << "\n\n";
  std::cout << "H_0:\n" << H_sac(0) << "\n";
  std::cout << "error: " << (H(0) - H_sac(0)).norm() << "\n\n";
  std::cout << "H_1:\n" << H_sac(1) << "\n";
  std::cout << "error: " << (H(1) - H_sac(1)).norm() << "\n\n";

  return 0;
}
