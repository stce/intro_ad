// naumann@stce.rwth-aachen.de

#include "Eigen/Dense"
#include <cmath>

using namespace std;

namespace {
  const int N = 4;
  const int M = 2;
}

/******************************************************************************
 * @brief Lighthouse function.
 *
 * @param[in] x Input vector x (size 4).
 * @param[out] y Output vector y (size 2).
 ******************************************************************************/
template <typename T> void F(
  const Eigen::Vector<T, N> &x,
  Eigen::Vector<T, M> &y
) {
  T v = x(2) * x(3);
  v = sin(v) / cos(v);
  T w = x(1) - v;
  y(0) = x(0) * v/w;
  y(1) = y(0) * x(1);
}

/******************************************************************************
 * @brief Lighthouse function SAC.
 *
 * @param[in] x Input vector x (size 4) (ν, γ, ω, t).
 * @param[out] y Output vector y (size 2).
 ******************************************************************************/
template <typename T> void F_sac(
  const Eigen::Vector<T, N> &x,
  Eigen::Vector<T, M> &y
) {
  // TODO: Exercise 1
}

/******************************************************************************
 * @brief Lighthouse function SAC first order tangent model.
 *
 * @param[in] x Input vector x (size 4) (ν, γ, ω, t).
 * @param[in] x_t1 Tangent input seeds.
 * @param[out] y Output vector y (size 2).
 * @param[out] y_t1 Output tangent (to harvest from).
 ******************************************************************************/
template <typename T> void F_sac_t1(
  const Eigen::Vector<T, N> &x,
  const Eigen::Vector<T, N> &x_t1,
  Eigen::Vector<T, M> &y,
  Eigen::Vector<T, M> &y_t1
) {
  // TODO: Exercise 2
}

/******************************************************************************
 * @brief Lighthouse function SAC second order tangent-over-tangent model.
 *
 * @param[in] x Input vector x (size 4) (ν, γ, ω, t).
 * @param[in] x_t2 Tangent input seeds (first direction).
 * @param[in] x_t1 Tangent input seeds (second direction).
 * @param[in] x_t1_t2 Tangent input seeds (2nd order).
 * @param[out] y Output vector y (size 2).
 * @param[out] y_t2 Output tangent (1st order, first direction).
 * @param[out] y_t1 Output tangent (1st order, second direction).
 * @param[out] y_t1_t2 Output tangent (2nd order).
 ******************************************************************************/
template <typename T> void F_sac_t1_t2(
  const Eigen::Vector<T, N> &x,
  const Eigen::Vector<T, N> &x_t2,
  const Eigen::Vector<T, N> &x_t1,
  const Eigen::Vector<T, N> &x_t1_t2,
  Eigen::Vector<T, M> &y,
  Eigen::Vector<T, M> &y_t2,
  Eigen::Vector<T, M> &y_t1,
  Eigen::Vector<T, M> &y_t1_t2
) {
  // TODO: Exercise 3
}
